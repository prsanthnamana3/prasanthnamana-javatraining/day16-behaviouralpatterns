package com.java.behavioral.patterns.chain;

public abstract interface Handler {
	
	void setSuccessor(Handler successor);
	
	void handleRequest(Request request);
	
}
