package com.java.behavioral.patterns.chain.demo;


import com.java.behavioral.patterns.chain.BaseHandler;
import com.java.behavioral.patterns.chain.RequestType;
import com.java.behavioral.patterns.chain.handlers.VP;
import com.java.behavioral.patterns.chain.Request;
import com.java.behavioral.patterns.chain.handlers.CEO;
import com.java.behavioral.patterns.chain.handlers.Director;

public class ChainOfResponsibilityDemo {

	public static void main(String[] args) {
		BaseHandler bryan = new Director();
		BaseHandler crystal = new VP();
		BaseHandler jeff = new CEO();
		
		bryan.setSuccessor(crystal);
		crystal.setSuccessor(jeff);
		
		Request request = new Request(RequestType.CONFERENCE, 500);
		bryan.handleRequest(request);
		
		request = new Request(RequestType.PURCHASE, 1000);
		bryan.handleRequest(request);
		
		request = new Request(RequestType.PURCHASE, 2000);
		bryan.handleRequest(request);
	}
}
