package com.java.behavioral.patterns.chain;

public enum RequestType {
	CONFERENCE, PURCHASE;
}
