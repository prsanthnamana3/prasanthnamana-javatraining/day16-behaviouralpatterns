package com.java.behavioral.patterns.chain.handlers;

import com.java.behavioral.patterns.chain.BaseHandler;
import com.java.behavioral.patterns.chain.Request;

public class CEO extends BaseHandler {

	@Override
	public void handleRequest(Request request) {

		System.out.println("CEOs can approve anything they want");
	}
}
