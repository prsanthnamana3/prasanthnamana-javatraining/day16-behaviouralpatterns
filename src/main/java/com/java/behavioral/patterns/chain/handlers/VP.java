package com.java.behavioral.patterns.chain.handlers;

import com.java.behavioral.patterns.chain.BaseHandler;
import com.java.behavioral.patterns.chain.Request;
import com.java.behavioral.patterns.chain.RequestType;

public class VP extends BaseHandler {

	@Override
	public void handleRequest(Request request) {
		if(request.getRequestType() == RequestType.PURCHASE) {
			if(request.getAmount() < 1500) {
				System.out.println("VPs can approve purchases below 1500");
			}
			else {
				successor.handleRequest(request);
			}
		}
	}
}
