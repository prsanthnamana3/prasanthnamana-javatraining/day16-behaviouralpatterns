package com.java.behavioral.patterns.chain;

public abstract class BaseHandler implements Handler{

    protected Handler successor;

    @Override
    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

}
