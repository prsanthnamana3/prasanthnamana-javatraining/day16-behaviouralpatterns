package com.java.behavioral.patterns.observer.clients;

import com.java.behavioral.patterns.observer.Observer;
import com.java.behavioral.patterns.observer.Subject;

public class TabletClient extends Observer {

	public TabletClient (Subject subject) {
		this.subject = subject;
		subject.attach(this);
	}

	//Optional
	public void addMessage(String message) {
		subject.setState(message + " - sent from tablet");
	}
	
	@Override
	public void update() {
		System.out.println("Tablet Stream: " + subject.getState());
	}

}
