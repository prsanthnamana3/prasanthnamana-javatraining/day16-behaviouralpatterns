package com.java.behavioral.patterns.practice.strategy;

public class Multiply extends Operator{
    public Multiply(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public int operate() {
        return a*b;
    }
}
