package com.java.behavioral.patterns.practice.strategy;

public class Add extends Operator{
    public Add(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public int operate() {
        return a+b;
    }
}
