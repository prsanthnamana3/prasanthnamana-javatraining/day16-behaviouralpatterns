package com.java.behavioral.patterns.practice.strategy;

public class Operation {
    private  Operator operator;
    public Operation (Operator opt){
        this.operator = opt;
    }
    public int operate(){
       return operator.operate();
    }
}
