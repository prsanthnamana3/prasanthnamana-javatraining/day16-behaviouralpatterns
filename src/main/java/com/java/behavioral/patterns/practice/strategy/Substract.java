package com.java.behavioral.patterns.practice.strategy;

public class Substract extends Operator{
    public Substract(int a, int b) {
        this.a = a;
        this.b = b;
    }
    @Override
    public int operate() {
        return a-b;
    }
}
