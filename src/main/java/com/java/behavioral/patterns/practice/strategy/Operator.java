package com.java.behavioral.patterns.practice.strategy;

public abstract class  Operator {
    int a;
    int b;
    public abstract int operate();
}
