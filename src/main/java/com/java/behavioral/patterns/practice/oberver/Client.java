package com.java.behavioral.patterns.practice.oberver;

public abstract class Client {
    protected PhoneMessageApp subject;

    public abstract void publish();
}
