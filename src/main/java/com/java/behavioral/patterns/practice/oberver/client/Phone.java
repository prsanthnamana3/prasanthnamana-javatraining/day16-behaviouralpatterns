package com.java.behavioral.patterns.practice.oberver.client;

import com.java.behavioral.patterns.practice.oberver.Client;
import com.java.behavioral.patterns.practice.oberver.PhoneMessageApp;

public class Phone extends Client {

    public Phone(PhoneMessageApp sub){
        this.subject = sub;
        subject.attach(this);
    }
    @Override
    public void publish() {
        System.out.println("Phone Client : " + subject.getMessage());
    }
}
