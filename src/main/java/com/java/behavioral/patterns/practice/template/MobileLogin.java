package com.java.behavioral.patterns.practice.template;

public class MobileLogin extends  Login{
    public MobileLogin() {
        super(false);
    }

    @Override
    public void processPassword() {
        System.out.println("Processed the password from mobile");
    }

    @Override
    public void encryptPassword() {
        System.out.println("Encrypted the password from mobile");
    }

    @Override
    public void savePassword() {
        System.out.println("Saved the password from mobile");
    }
}
