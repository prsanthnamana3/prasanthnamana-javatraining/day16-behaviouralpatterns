package com.java.behavioral.patterns.practice.template;

public class WebLogin extends Login{
    public WebLogin(){
        super(true);
    }
    @Override
    public void processPassword() {
        System.out.println("Processed the password from Web");
    }

    @Override
    public void encryptPassword() {
        System.out.println("Encrypted the password from Web");
    }

    @Override
    public void savePassword() {
        System.out.println("Saved the password from Web");
    }
}
