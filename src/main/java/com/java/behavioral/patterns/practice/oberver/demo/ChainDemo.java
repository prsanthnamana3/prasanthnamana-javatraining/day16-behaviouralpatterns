package com.java.behavioral.patterns.practice.oberver.demo;

import com.java.behavioral.patterns.practice.oberver.PhoneMessageApp;
import com.java.behavioral.patterns.practice.oberver.client.Email;
import com.java.behavioral.patterns.practice.oberver.client.Phone;

public class ChainDemo {
    public static void main(String[] args){
        PhoneMessageApp pm = new PhoneMessageApp();
        Phone phone = new Phone(pm);
        Email email  = new Email(pm);
        pm.setMessage("This is my 1st message");
        pm.setMessage("This is my second message");
        pm.detach(phone);
        pm.setMessage("This is my 3rd message");

    }
}
