package com.java.behavioral.patterns.practice.template;

public abstract class Login {

    private final boolean isWebLogin;
    public Login(boolean isWebLogin){
        this.isWebLogin = isWebLogin;
    }
    public abstract void processPassword();
    public abstract void encryptPassword();
    public abstract void savePassword();
    public final void saveDevice(){
        System.out.println("This device is saved in DB");
    }

    public final void processLogin(){
        processPassword();
        encryptPassword();
        savePassword();
        if(!isWebLogin)saveDevice();
    }
}
