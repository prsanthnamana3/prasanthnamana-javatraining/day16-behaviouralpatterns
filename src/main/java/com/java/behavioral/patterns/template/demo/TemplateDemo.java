package com.java.behavioral.patterns.template.demo;

import com.java.behavioral.patterns.template.OrderTemplate;
import com.java.behavioral.patterns.template.WebOrder;
import com.java.behavioral.patterns.template.StoreOrder;

public class TemplateDemo {

	public static void main(String[] args) {
		System.out.println("Web Order:");
		
		OrderTemplate webOrder = new WebOrder();
		webOrder.processOrder();
		
		System.out.println("\nStore Order:");
		
		OrderTemplate storeOrder = new StoreOrder(true);
		storeOrder.processOrder();
	}
}
